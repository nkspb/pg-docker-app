**Deploy app automatically:**
1. Make changes to local repository and commit
2. Tag with app version:<br />
_git tag <major_ver>.<minor_ver>_<br />
3. Push to repository
_git push origin <major_ver>.<minor_ver>_

App gets pushed to Docker Hub with tag specified, and deployed to k8s via _helm/charts/pg-app_

**Deploy app manually:**
1. Make sure all prerequisites from infra-prep are complete:
https://gitlab.com/nkspb/infra-prep/-/blob/main/README.md

2. Run postgres chart first because app connects to it:
_cd helm/charts/postgres_
_helm upgrade --install --create-namespace --namespace pg-app postgres ._

_cd helm/postgres/charts/pg-app_
_helm upgrade --install --create-namespace --namespace pg-app pg-app --set appVersion=X ._<br />
where _appVersion_ is the version on Docker Hub:
https://hub.docker.com/repository/docker/nkom/pg-app
